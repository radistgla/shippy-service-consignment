module gitlab.com/radistgla/shippy-service-consignment

go 1.13

require (
	github.com/golang/protobuf v1.3.3
	github.com/micro/go-micro v1.18.0
	github.com/micro/go-micro/v2 v2.0.0 // indirect
	github.com/micro/protobuf v0.0.0-20180321161605-ebd3be6d4fdb // indirect
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
)
